# [WiPS - WiFi Power Switch](https://bitbucket.org/Rmic/wips)

This project is based on [nodemcu-httpserver](https://github.com/marcoskirsch/nodemcu-httpserver) - a simple web server written in Lua for the ESP8266 running the NodeMCU firmware.

License is GNU GPL v2.

Before uploading the code please copy httpserver-conf.lua.template to httpserver-conf.lua and change it according to your needs.



