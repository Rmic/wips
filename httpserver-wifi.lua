-- httpserver-wifi.lua
-- Part of nodemcu-httpserver, configures NodeMCU's WiFI in boot.
-- Author: Marcos Kirsch

local conf = nil
if file.exists("httpserver-conf.lc") then
   conf = dofile("httpserver-conf.lc")
else
   if not file.exists("httpserver-conf.lua") then
      print ("ERROR: No configuration file found. Please upload httpserver-conf.lua with you settings. Look at httpserver-conf.lua.template for example.")
      return false
   end
   conf = dofile("httpserver-conf.lua")
end

wifi.setmode(conf.wifi.mode)

if (conf.wifi.mode == wifi.SOFTAP) or (conf.wifi.mode == wifi.STATIONAP) then
    print('AP MAC: ',wifi.ap.getmac())
    wifi.ap.config(conf.wifi.accessPoint.config)
    wifi.ap.setip(conf.wifi.accessPoint.ip)
end

if (conf.wifi.mode == wifi.STATION) or (conf.wifi.mode == wifi.STATIONAP) then
    print('Client MAC: ',wifi.sta.getmac())
    station_cfg={}
    station_cfg.ssid=conf.wifi.station.ssid
    station_cfg.pwd=conf.wifi.station.pwd
    station_cfg.auto=true
	station_cfg.save=false
    wifi.sta.config(station_cfg)
end

print('chip: ',node.chipid())
print('heap: ',node.heap())

conf = nil
collectgarbage()

return true

-- End WiFi configuration
