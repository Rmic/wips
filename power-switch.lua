-- GPIO 1, 2 - relays, activated by low level;
-- GPIO 5, 6, 7 - red, green, blue LEDs of RGB LED;
-- GPIO 3, 8 - output power LEDs;

local pswitch = {}

local RELAY_0 = 1
local RELAY_1 = 2
local LED_0 = 3
local LED_1 = 8

local LED_RED = 5
local LED_GREEN = 6
local LED_BLUE = 7

-- What relays are normally closed (so that they output 220V voltage while relay's coil is not activated).
-- This is determined by hardware connection.
local RELAY_0_NC = false
local RELAY_1_NC = true

local LED_STATE = {}
LED_STATE[false] = gpio.LOW
LED_STATE[true] = gpio.HIGH

local RELAY_STATE = {}
RELAY_STATE[false] = gpio.HIGH
RELAY_STATE[true] = gpio.LOW



function pswitch.init()
    gpio.mode(RELAY_0, gpio.OUTPUT)
    gpio.mode(RELAY_1, gpio.OUTPUT)
    
    gpio.mode(LED_RED, gpio.OUTPUT)
    gpio.mode(LED_GREEN, gpio.OUTPUT)
    gpio.mode(LED_BLUE, gpio.OUTPUT)
    
    gpio.mode(LED_0, gpio.OUTPUT)
    gpio.mode(LED_1, gpio.OUTPUT)
    
    gpio.write(RELAY_0, RELAY_STATE[false])
    gpio.write(RELAY_1, RELAY_STATE[false])
    
    gpio.write(LED_RED, gpio.LOW)
    gpio.write(LED_GREEN, gpio.LOW)
    gpio.write(LED_BLUE, gpio.LOW)
    
    gpio.write(LED_0, LED_STATE[RELAY_0_NC])
    gpio.write(LED_1, LED_STATE[RELAY_1_NC])

end

function pswitch.set_output(output_index, output_state)

    if (output_index == 0) then
        gpio.write(RELAY_0, RELAY_STATE[RELAY_0_NC ~= output_state])
        gpio.write(LED_0, LED_STATE[output_state])
    elseif (output_index == 1) then
        gpio.write(RELAY_1, RELAY_STATE[RELAY_1_NC ~= output_state])
        gpio.write(LED_1, LED_STATE[output_state])
    end
end


function pswitch.set_outputs(out0_state, out0_interval, out1_state, out1_interval)

    curr_states = pswitch.get_outputs()
    curr0 = curr_states[0] 
    curr1 = curr_states[1]
    
    if out0_state ~= nil then
        if (curr0 ~= out0_state) then
            pswitch.set_output(0, out0_state)
            if (out0_interval ~= nil) then
                pswitch.set_output_delayed(0, curr0, out0_interval)
            end
        end
    end
    
    if out1_state ~= nil then
        if (curr1 ~= out1_state) then
            pswitch.set_output(1, out1_state)
            if (out1_interval ~= nil) then
                pswitch.set_output_delayed(1, curr1, out1_interval)
            end
        end
    end
    
end

function pswitch.get_outputs()
    r0_on = RELAY_STATE[true] == gpio.read(RELAY_0)
    r1_on = RELAY_STATE[true] == gpio.read(RELAY_1)
    return { [0] = (r0_on ~= RELAY_0_NC),
             [1] = (r1_on ~= RELAY_1_NC)
           }
end

function pswitch.on_connected(connected)
    gpio.write(LED_GREEN, LED_STATE[connected])
    gpio.write(LED_RED, LED_STATE[not connected])
end

local timers = {}
function pswitch.set_output_delayed(output_index, output_state, time_ms)

    local t = timers[output_index]
    if (t ~= nil) then
        t:unregister()
    end
    
    if (time_ms < 1000) then time_ms = 1000 end
    if (time_ms > 30000) then time_ms = 30000 end
    
    t = tmr.create()
    t:register(time_ms, tmr.ALARM_SINGLE, function (timer)
        timer:unregister()
        timers[output_index] = nil
        pswitch.set_output(output_index, output_state)
    end)
    
    timers[output_index] = t
    t:start()
end
    
return pswitch
