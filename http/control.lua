return function (connection, req, args)
   dofile("httpserver-header.lc")(connection, 200, 'html')
   connection:send([===[
   <!DOCTYPE html><html lang="en"><head><meta charset="utf-8">
   <title>WiPS - outputs control</title></head><body><h1>WiPS outputs control</h1>
   ]===])

   pswitch = require "power-switch"
   

    local r0 = nil
    local r1 = nil


    if args.r0 == 'on' then
        r0 = true
    elseif args.r0 == 'off' then
        r0 = false
    end

    if args.r1 == 'on' then
        r1 = true
    elseif args.r1 == 'off' then
        r1 = false
    end
    
    pswitch.set_outputs(r0, tonumber(args.r0_time), r1, tonumber(args.r1_time))
    
    outputs_status = pswitch.get_outputs()
   
    on_off = { [false] = 'off', [true] = 'on' }
   
    connection:send('<h2>Current output status</h2>')
    connection:send('<li><b>Output #0:</b> ' .. on_off[outputs_status[0]] .. "<br></li>\n")
    connection:send('<li><b>Output #1:</b> ' .. on_off[outputs_status[1]] .. "<br></li>\n")
   
    connection:send('<br>')
   
    connection:send('<h2>Modify output status</h2>')
   
        connection:send([===[

    <form method="GET">
        Output #0:
        <br>
            <input type="radio" name="r0" value="null" checked>Unchanged
            <input type="radio" name="r0" value="on">On
            <input type="radio" name="r0" value="off">Off
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="checkbox" id="r0_checkbox">Temporary for
            <input type="number" min="1000" max="30000" value="5000" name="r0_time" disabled="true" id="r0_time">Interval(ms)
            
        <br>
        Output #1:
        <br>        
            <input type="radio" name="r1" value="null" checked>Unchanged
            <input type="radio" name="r1" value="on">On
            <input type="radio" name="r1" value="off">Off
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="checkbox" id="r1_checkbox">Temporary for
            <input type="number" min="1000" max="30000" value="5000" name="r1_time" disabled="true" id="r1_time">Interval(ms)
        <br>
             <input type="submit" value ="Send">
        
    </form>

    <script>
        document.getElementById('r0_checkbox').onchange = function() {
            document.getElementById('r0_time').disabled = !this.checked;
        };
        document.getElementById('r1_checkbox').onchange = function() {
            document.getElementById('r1_time').disabled = !this.checked;
        };
    </script>
         
      ]===])

   connection:send("</body></html>")
end

