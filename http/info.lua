local function sendAttr(connection, attr, val, unit)
   --Avoid error when Nil is in atrib=val pair.
   if not attr or not val then
      return
   else
      if unit then
         unit = ' ' .. unit
   else
      unit = ''
   end
      connection:send("<li><b>".. attr .. ":</b> " .. val .. unit .. "<br></li>\n")
   end
end

return function (connection, req, args)
   dofile("httpserver-header.lc")(connection, 200, 'html')
   connection:send('<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>WiPS - WiFi Power Switch</title></head><body>')
   majorVer, minorVer, devVer, chipid, flashid, flashsize, flashmode, flashspeed = node.info();
   connection:send("<h1>Firmware and hardware info</h1>")
   connection:send("<ul>")
   sendAttr(connection, "NodeMCU version"       , majorVer.."."..minorVer.."."..devVer)
   sendAttr(connection, "chipid"                , chipid)
   sendAttr(connection, "flashid"               , flashid)
   sendAttr(connection, "flashsize"             , flashsize, 'Kb')
   sendAttr(connection, "flashmode"             , flashmode)
   sendAttr(connection, "flashspeed"            , flashspeed / 1000000 , 'MHz')
   sendAttr(connection, "heap free"             , node.heap() , 'bytes')
   sendAttr(connection, 'Memory in use'         , collectgarbage("count") , 'KB')
   connection:send("</ul>")
   
   connection:send("<h1>Connection info</h1>")
   connection:send("<ul>")
   ip, subnetMask = wifi.sta.getip()
   config = wifi.sta.getconfig(true)
   rssi=wifi.sta.getrssi()
   sendAttr(connection, 'WiFi SSID', config.ssid)
   sendAttr(connection, 'RSSI', rssi)
   sendAttr(connection, 'Station IP address'    , ip)
   sendAttr(connection, 'Station subnet mask'   , subnetMask)
   sendAttr(connection, 'MAC address'           , wifi.sta.getmac())
   local mdns_link = nil
   if (mdns) then
     local conf = dofile('httpserver-conf.lc')
     local mdns_url = 'http://' .. conf['mdns']['hostname'] .. '.local:' .. conf['general']['port']
     mdns_link = '<a href=' .. mdns_url .. '>' .. mdns_url .. '</a>'
   end
   sendAttr(connection, 'mDNS link' , mdns_link)
   
   sendAttr(connection, 'Auth user'             , req.user) 
  
   connection:send("</ul>")
   connection:send('</body></html>')
   
   collectgarbage()
end
