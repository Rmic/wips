-- Compile freshly uploaded nodemcu-httpserver lua files.
if file.exists("httpserver-compile.lc") then
   dofile("httpserver-compile.lc")
else
   dofile("httpserver-compile.lua")
end

local pswitch = require "power-switch"
pswitch.init()

-- Set up NodeMCU's WiFi
if dofile("httpserver-wifi.lc") then

	-- Start nodemcu-httpsertver
	dofile("httpserver-init.lc")
	
end
