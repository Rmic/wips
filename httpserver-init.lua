-- httpserver-init.lua
-- Part of nodemcu-httpserver, launches the server.
-- Author: Marcos Kirsch

-- Function for starting the server.
-- If you compiled the mdns module, then it will also register with mDNS.

local sv = nil
local pswitch = require "power-switch"

local startServer = function(ip)
   local conf = dofile('httpserver-conf.lc')
   
   sv = dofile("httpserver.lc")(conf['general']['port'])
   if (sv) then
      print("nodemcu-httpserver running at:")
      print("   http://" .. ip .. ":" ..  conf['general']['port'])
      if (mdns) then
         mdns.register(conf['mdns']['hostname'], { description=conf['mdns']['description'], service="http", port=conf['general']['port'], location=conf['mdns']['location'] })
         print ('   http://' .. conf['mdns']['hostname'] .. '.local.:' .. conf['general']['port'])
      end
   end
   conf = nil
end

if (wifi.getmode() == wifi.STATION) or (wifi.getmode() == wifi.STATIONAP) then

   -- Connect to the WiFi access point and start server once connected.
   -- If the server loses connectivity, server will restart.
   wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, function(args)
      print("Connected to WiFi Access Point. Got IP: " .. args["IP"])
	  pswitch.on_connected(true)
      startServer(args["IP"])      
   end)
   
   wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, function(args)
         print("Lost connectivity!")
		 pswitch.on_connected(false)
		 if (sv ~= nil) then
			sv:close()
			sv = nil
		 end
      end)

else

   startServer(wifi.ap.getip())

end
